# NOTE: in a full solution, we would initialize this array only once.
# In addition, we would perform once and then hold in memory the
# transformations necessary for use in the CodeGenerator class.

DISTRACTING_WORDS = %w(
  darn
  rats
  egg
  fuzzy
  kthx
  haha
  ugh
  777
  cheese
)
