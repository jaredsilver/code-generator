require './code_generator'

describe CodeGenerator do
  let(:code) { CodeGenerator.call(options) }
  let(:options) { {} }

  context 'when code length is not specified' do
    it 'generates a code of length 6' do
      expect(code.length).to be(6)
    end
  end

  context 'when code length is specified' do
    let(:options) { { code_length: 12 } }

    it 'generates a code of specified length' do
      expect(code.length).to be(12)
    end
  end

  context 'when characters are excluded' do
    let(:options) { { excluded_characters: ('B'..'Z').to_a + ('0'..'9').to_a } }

    it 'generates a code without excluded characters' do
      expect(code).to eq('AAAAAA')
    end
  end

  context 'when checking for a distracting sequence' do
    before { srand 0 } # seed random so we always generate the code "PSYAD6"

    context 'when the code contains a distracting sequence' do
      let(:options) { { distracting_words: ['psa'] } }

      it 'generates a new code' do
        expect(code).not_to eq('PSYAD6')
      end
    end

    context 'when the code does not contain a distracting sequence' do
      let(:options) { { distracting_words: [] } }

      it 'generates the expected code' do
        expect(code).to eq('PSYAD6')
      end
    end
  end

end
