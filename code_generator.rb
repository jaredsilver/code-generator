require 'procto'
require 'dry-initializer'
require './distracting_words'

class CodeGenerator
  include Procto.call
  extend Dry::Initializer

  option :code_length, default: proc { 6 }
  option :excluded_characters, default: proc { ['I', 'L', '1', '0', 'O'] }
  option :distracting_words, default: proc { DISTRACTING_WORDS }

  def call
    generate_code
    generate_code while contains_distracting_words
    @code
  end

  private

  def generate_code
    @code = String.new
    code_length.times { @code << characters.sample }
  end

  def characters
    @characters ||= ('A'..'Z').to_a + ('0'..'9').to_a - excluded_characters
  end

  def contains_distracting_words
    distracting_words.each do |word|
      return true if Regexp.new(word.upcase.chars.join('.*')).match(@code)
    end

    false
  end
end
